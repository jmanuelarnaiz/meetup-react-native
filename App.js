import React, { Component } from 'react';
import { View, ActivityIndicator, StyleSheet } from 'react-native';

import AppNavigation from './AppNavigation';
import Auth from './src/components/Auth';
import { auth } from './src/config/firebase';

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: null,
      isLoading : true
    }
  }

  componentWillMount() {
      /* En emacs6, si el valor es igual a la propiedad, se puede dejar igual. Esto es;
      user: user pasa a ser user directamente. */
      auth.onAuthStateChanged( (user) => {
          this.setState({ user, isLoading:false })
      })
  }

  render() {
    return this.state.isLoading
    ? (<ActivityIndicator style={styles.loader} size={100} color="red" />)
    : (
      <View style={{ flex: 1 }}>
        {
          this.state.user
          ? <AppNavigation user={ this.state.user } />
          : <Auth />
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
    loader: {
        marginTop: 100
    }
})
