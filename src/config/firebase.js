import * as firebase from 'firebase'
import { YellowBox } from 'react-native';
import _ from 'lodash';

export const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyB3S-vaGmIUzFoNhHIEGfvRujeHASpGU-U",
    authDomain: "react-native-meetup-ejemplo.firebaseapp.com",
    databaseURL: "https://react-native-meetup-ejemplo.firebaseio.com",
    projectId: "react-native-meetup-ejemplo",
    storageBucket: "",
    messagingSenderId: "924847475765"
})

// Para evitar el warning del timer
YellowBox.ignoreWarnings(['Setting a timer']);
const _console = _.clone(console);
console.warn = message => {
  if (message.indexOf('Setting a timer') <= -1) {
    _console.warn(message);
  }
};

export const db = firebaseApp.database();
export const auth = firebaseApp.auth();

export default firebase;
