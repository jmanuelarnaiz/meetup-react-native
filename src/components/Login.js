import React from 'react';
import { View, Text, TextInput, Image, TouchableHighlight, StyleSheet, Alert } from 'react-native';
import { navigationOptions } from '../config/navOptions';
import { auth } from '../config/firebase';
import { authStyles } from '../styles/authStyles';

export default class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password:''
        }
    }

    static navigationOptions = ( {navigation} ) => ({
        tabBarLabel: 'Accede con tu cuenta',
        ...navigationOptions
    });

    /* Al usar una arrow function, ya no hace falta bindearlo. Ya sabe que
    pertenece a la función de la clase y no al componente.
    Si no usarámos la función de arrow, se pensaría que pertenece al componente
    desde el que se llama, en este caso, TouchableHighlight. Minuto 16:15 del video 6 */
    authenticateUser(email, password) {
        auth.signInWithEmailAndPassword(email, password).catch( (error) => {
            const errorCode = error.code;
            const errorMessage = error.message;

            console.error('Error Loggin in. Code: ', errorCode, ' Message: ', errorMessage);

            Alert.alert(
                'Error en el inicio de sesión',
                'Ha habido un error al iniciar sesión: Código de error: ' + errorMessage,
                [
                    {text: 'OK', onPress: () => console.log('OK Pressed')}
                ],
                { cancelable: true }
            )
        });
    }

    render() {
        return (
            <View style={ authStyles.container }>
                <Image style={ authStyles.logo } source={require('../assets/logo-meetup.png') } />
                <TextInput style={ authStyles.input }
                    onChangeText={ (text) => this.setState( {email: text} )}
                    placeholder={"Tu email..."}
                    value={this.state.email}
                />
                <TextInput style={ authStyles.input }
                    secureTextEntry={ true }
                    onChangeText={ (text) => this.setState( {password: text} )}
                    placeholder={"Tu contraseña..."}
                    value={this.state.password}
                />
            <TouchableHighlight
                style={ authStyles.loginBtn }
                onPress={() => this.authenticateUser(this.state.email, this.state.password)}
                >
                <View>
                    <Text style={ authStyles.textBtn }>Accede con tu cuenta</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={ () => this.props.navigation.navigate('SignUp')}>
                <View>
                    <Text style={{ textAlign: 'center'}}>
                        Crea tu cuenta aquí
                    </Text>
                </View>
            </TouchableHighlight>
            </View>
        );
    }
}
