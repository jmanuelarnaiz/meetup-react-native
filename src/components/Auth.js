import React from 'react';
import { TabNavigator } from 'react-navigation';

import { navigationOptions } from '../config/navOptions';
import Login from './Login';
import SignUp from './SignUp';

const Auth = TabNavigator({
    Login: { screen: Login },
    SignUp: { screen: SignUp }
}, {
    tabBarOptions: {
        style: {
            backgroundColor: 'red',
            height: 120,
            paddingVertical: 25
        },
        indicatorStyle: {
            backgroundColor: '#eaeaea'
        }
    }
})

export default Auth;
