import React from 'react';
import { View, Text, TextInput, Image, TouchableHighlight, StyleSheet } from 'react-native';
import { navigationOptions } from '../config/navOptions';
import { auth, db } from '../config/firebase';
import { authStyles } from '../styles/authStyles';
import { generateAvatarURL } from '../utils/gravatar';

export default class SignUp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password:''
        }
    }

    static navigationOptions = ( {navigation} ) => ({
        tabBarLabel: 'Regístrate',
        ...navigationOptions
    });

    createUser(email, password) {
        auth.createUserWithEmailAndPassword(email, password)
            .then( (result) => {
                db.ref(`/users/${result.uid}`).set({
                    uid: result.uid,
                    email: result.email,
                    avatar: generateAvatarURL()
                })
            })
    }

    render() {
        return (
            <View style={ authStyles.container }>
                <Image style={ authStyles.logo } source={require('../assets/logo-meetup.png') } />
                <TextInput style={ authStyles.input }
                    onChangeText={ (text) => this.setState( {email: text} )}
                    placeholder={"Tu email..."}
                    value={this.state.email}
                />
                <TextInput style={ authStyles.input }
                    secureTextEntry={ true }
                    onChangeText={ (text) => this.setState( {password: text} )}
                    placeholder={"Tu contraseña..."}
                    value={this.state.password}
                />
            <TouchableHighlight
                style={ authStyles.loginBtn }
                onPress={() => this.createUser(this.state.email, this.state.password)}
                >
                <View>
                    <Text style={ authStyles.textBtn }>Crea una cuenta nueva</Text>
                </View>
            </TouchableHighlight>
            <TouchableHighlight onPress={ () => this.props.navigation.navigate('Login')}>
                <View>
                    <Text style={{ textAlign: 'center'}}>
                        ¿Tienes cuenta? Accede desde aquí
                    </Text>
                </View>
            </TouchableHighlight>
            </View>
        );
    }
}
