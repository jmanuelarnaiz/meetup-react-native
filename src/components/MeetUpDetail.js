import React from 'react';
import { ScrollView, View, Text, Image, TouchableHighlight, StyleSheet } from 'react-native';

import { navigationOptions } from '../config/navOptions';
import { FontAwesome } from '@expo/vector-icons';
import moment from 'moment';
import 'moment/locale/es';

import { db, auth } from '../config/firebase';

export default class MeetUpDetail extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            event: {},
            assistants: []
        }
    }

    static navigationOptions = ( {navigation} ) => ({
        title: `Evento de ${navigation.state.params.group}`,
        ...navigationOptions
    });

    componentWillMount() {
        const { navigation } = this.props;
        moment.locale('es');

        db.ref(`/events/${ navigation.state.params.id }`)
          .once('value', snapshot => {
              this.setState({
                  event: snapshot.val()
              })
          })
          // 'Once' es como hacer una query, y 'on' es un listener
          db.ref(`/eventAttendees/${ navigation.state.params.id }`)
            .on('child_added', (snapshot) => { // Cada vez que se añada un usuario
                this.setState({
                    assistants: this.state.assistants.concat(snapshot.val())
                })
            })

            // Child added child_added se activa una vez por cada campo secundario
            // y cada vez que se agregue un nuevo campo secundario en la ruta especificada

            db.ref(`/eventAttendees/${ navigation.state.params.id }`)
              .on('child_removed', (snapshot) => { // Cada vez que se elimina un usuario
              console.log("Asistentes antes: ", this.state.assistants);
                  this.setState({
                      assistants: this.state.assistants.filter( (assistant) => assistant === snapshot.val())
                  })
              })
    }

    componentWillUnmount() {
        /* Es conveniente que, por motivos de rendimiento, si salimos de esta vista,
        dejemos de escuchar con el 'on' */

        const { navigation } = this.props;
        db.ref(`/eventAttendees/${ navigation.state.params.id }`).off();
        db.ref('/eventAttendees/').off();
    }

    booking() {
        // Recupera los datos del usuario de Firebase por defecto (solo el correo y el UID)
        const userUID = auth.currentUser.uid;
        const eventID = this.props.navigation.state.params.id;

        db.ref(`/users/${userUID}`)
          .once('value', (snapshot) => {
              const { email, uid, avatar } = snapshot.val();

              /* Guardamos en la colección de eventAttendees lo siguiente:
              Eventos por su ID, y en cada evento, los usuarios por su ID,
              almacenando email, uid y avatar */
              db.ref('/eventAttendees')
                .child(eventID) // Para acceder al hijo
                .child(userUID)
                .set({ email, uid, avatar })
          })
    }

    cancelBooking() {
        const userUID = auth.currentUser.uid;
        const eventID = this.props.navigation.state.params.id;

        db.ref('/eventAttendees')
          .child(eventID)
          .remove()
    }

    render() {
        const { navigation } = this.props;
        let isRegisteredInEvent;
        let bookingButton;
        if (this.state.assistants.some( (assistant) => (assistant.uid === auth.currentUser.uid))) {
            isRegisteredInEvent = true;
        }

        if (isRegisteredInEvent) {
            bookingButton = (
                <TouchableHighlight style={ styles.rsvpBtnCancel } onPress={() => this.cancelBooking()}>
                    <View>
                        <Text style={ styles.rsvpText }>
                            Desinscribirme
                        </Text>
                    </View>
                </TouchableHighlight>
            );
        } else {
            bookingButton = (
                <TouchableHighlight style={ styles.rsvpBtn } onPress={() => this.booking()}>
                    <View>
                        <Text style={ styles.rsvpText }>
                            Inscribirme
                        </Text>
                    </View>
                </TouchableHighlight>
            );
        }
        return (
            <ScrollView style={ styles.container }>
                <Image style={ styles.coverImage } source={require('../assets/meetup-event.jpg')} />
                <Text style={ styles.title}>{ this.state.event.title }</Text>
                <View style={ styles.info }>
                    <View>
                    <Text style={ styles.infoText }>
                        { moment(this.state.event.date).fromNow() }
                    </Text>
                    <Text style={ styles.infoSubText }>
                        { moment(this.state.event.date).format('dddd, DD MMMM') }
                    </Text>
                    </View>
                </View>
                <View style={ styles.info }>
                    <FontAwesome style={ styles.infoIcon } name="calendar-o" size={20} color="grey" />
                    <View>
                        <Text style={ styles.infoText }>{ this.state.event.location }</Text>
                        <Text style={ styles.infoSubText }>{ this.state.event.locationAddress }</Text>
                    </View>
                </View>
                { /* https://reactjs.org/docs/conditional-rendering.html#inline-if-with-logical--operator */ }
                { this.state.assistants.length > 0 && (
                    <View style={ styles.info }>
                        { this.state.assistants.map( (attendee, i) => (
                            <Image
                                key={i}
                                style={ styles.imageAvatar }
                                source={{ uri: attendee.avatar }} />
                        ))}
                    </View>
                )}
                <View style={ styles.info }>{ bookingButton }</View>
                <View style={ styles.description }>
                    <Text style={ styles.descriptionText }>
                        { this.state.event.description }
                    </Text>
                </View>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    coverImage: {
        flex:1,
        height: 300
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        marginHorizontal: 20,
        marginVertical: 10
    },
    info: {
        flexDirection: 'row',
        marginHorizontal: 20,
        paddingTop: 10
    },
    infoIcon: {
        margin: 10
    },
    infoText: {
        color: 'grey',
        fontSize: 17
    },
    infoSubText: {
        color: 'black',
        fontSize: 13
    },
    rsvpBtn: {
        flex: 1,
        backgroundColor: 'green',
        margin: 15,
        padding: 10,
        borderRadius: 3
    },
    rsvpBtnCancel: {
        flex: 1,
        backgroundColor: 'red',
        margin: 15,
        padding: 10,
        borderRadius: 3
    },
    rsvpText: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold'
    },
    imageAvatar: {
        width: 32,
        height: 32,
        borderRadius: 16,
        margin: 5
    },
    description: {
        backgroundColor: '#eaeaea',
        borderTopWidth: 1,
        borderColor: "#aaa",
        padding: 20,
        marginTop: 10
    },
    descriptionText: {
        fontSize: 17
    }
})
