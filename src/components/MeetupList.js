import React from 'react';
import {
    View,
    ScrollView,
    Text,
    Image,
    TouchableWithoutFeedback,
    StyleSheet,
    Dimensions,
    ActivityIndicator
} from 'react-native';

import MeetUpCard from './MeetUpCard';
import { navigationOptions } from '../config/navOptions';
import { db } from '../config/firebase';

const { width, height } = Dimensions.get('window');

export default class MeetupList extends React.Component {
    constructor(props) {
        super(props);
        console.ignoredYellowBox = [
            'Setting a timer'
        ];
        this.state = {
            events: [],
            isLoading : true
        }
    }

    static navigationOptions = ( {navigation} ) => ({
        title: 'Meetup',
        ...navigationOptions // Spread operator
    });

    componentWillMount() {
  //     return fetch('http://localhost:3000/events')
  //     .then((response) => response.json())
  //     .then((responseJson) => {
  //       this.setState({
  //         events: this.events.concat(snapshot.val()),
  //         isLoading: false
  //       });
  // })
  // .catch((error) =>{
  //   console.error(error);
  //   this.setState({
  //     events: [],
  //     isLoading: false
  //   });
  // });
        /*
        db.ref('/events')
        .once('value').then(snapshot => {
            this.setState({
                events: this.events.concat(snapshot.val()),
                isLoading: false
            })
        });
        */

        db.ref('/events')
        .once('value', (snapshot) => {
            // isLoading: false
            this.setState({
                events: this.state.events.concat(snapshot.val()),
                isLoading: false
            })
        })
    }

    /*
        Cada vez que iteremos sobre un array de componentes, estos han de tener una
        propiedad key ÚNICA.
    */

    render() {
        const { navigation } = this.props;
        return this.state.isLoading
        ? (<ActivityIndicator style={styles.loader} size={100} color="red" />)
        : (<ScrollView style={ styles.container } >
                { this.state.events.map((event, i) => (
                    <MeetUpCard
                        key={ i }
                        navigation={ this.props.navigation }
                        event={ event } />
                ))}
            </ScrollView>)
    }
}

/*
    En EmacsScript 6, si el atributo es igual a la propiedad, lo siguiente
    se puede convertir. Pasa de 'height: height' a únicamente 'height'
*/

const styles = StyleSheet.create({
    loader: {
        marginTop: 100
    },
    container: {
        flex: 1,
        margin: 10,
        height
    }
})
