import React from 'react';
import { StyleSheet } from 'react-native';

export const authStyles = {
    container: {
        flex: 1,
        margin: 30,
        marginTop: 90
    },
    logo: {
        width: 240,
        height: 120,
        marginBottom: 20
    },
    input: {
        padding: 10,
        fontSize: 18,
        borderColor: 'orangered'
    },
    loginBtn: {
        backgroundColor: 'orange',
        marginVertical: 20,
        marginHorizontal: 10,
        padding: 10,
        borderRadius: 3
    },
    textBtn: {
        color: 'white',
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center'
    }
}
